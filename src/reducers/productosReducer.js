import {
    AGREGAR_PRODUCTO,
    AGREGAR_PRODUCTO_EXITO,
    AGREGAR_PRODUCTO_ERROR,
    OBTENER_PRODUCTOS_PETICION,
    OBTENER_PRODUCTOS_EXITO,
    OBTENER_PRODUCTOS_ERROR,
    OBTENER_PRODUCTO_ELIMINAR,
    OBTENER_PRODUCTO_ELIMINAR_EXITO,
    OBTENER_PRODUCTO_ELIMINAR_ERROR,
    OBTENER_PRODUCTO_EDITAR,
    OBTENER_PRODUCTO_EDITAR_ERROR,
    OBTENER_PRODUCTO_EDITAR_EXITO
} from '../types'
import clienteAxios from "../config/axios";

//Cada reducer tiene su propio state
const initialState = {
    productos       : [],
    error           : null,
    loading         : false,
    productoEliminar: null,
    productoEditar  : null,
};

export default function( state = initialState, action ){

    switch( action.type ){
        case AGREGAR_PRODUCTO:
            return {...state, loading: action.payload};
        case AGREGAR_PRODUCTO_EXITO:
            return {
                ...state,
                loading: false,
                payload: [...state.productos, action.payload]
            };
        case OBTENER_PRODUCTOS_ERROR:
        case AGREGAR_PRODUCTO_ERROR:
        case OBTENER_PRODUCTO_ELIMINAR_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case OBTENER_PRODUCTOS_PETICION:
            return {
                ...state, loading: action.payload
            };
        case OBTENER_PRODUCTOS_EXITO:
            return {
                ...state,
                loading: false,
                error: false,
                productos: action.payload
            };
        case OBTENER_PRODUCTO_ELIMINAR:
            return {
                ...state, productoEliminar: action.payload
            };
        case OBTENER_PRODUCTO_ELIMINAR_EXITO:
            return {
                ...state,
                productos: state.productos.filter( producto => producto.id !== state.productoEliminar ),
                productoEliminar: null
            };
        case OBTENER_PRODUCTO_EDITAR:
            return {
                ...state,
                productoEditar: action.payload
            };
        default:
            return state;
    }

}