import React from "react";
import {Link, useHistory } from "react-router-dom";
import { useDispatch} from "react-redux";
import Swal from "sweetalert2";
import {borrarProductoAction, obtenerProductoEditar} from "../actions/productoActions";

const Producto = ({ producto }) => {

    const { nombre, precio, id } = producto;

    const dispatch = useDispatch();
    const history  = useHistory(); //habilitar history para redireccion

    const alertEliminar = id => {

        Swal.fire({
            title   : 'Desea eliminar este producto?',
            text    : 'Eliminar producto permanentemente',
            icon    : 'warning',
            showCancelButton : true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar'
        }).then((result) => {
            if( result.value ){
                dispatch( borrarProductoAction(id) );
                Swal.fire(
                    'Prdocuto eliminado',
                    '',
                    'success'
                );
            }
        });

    };

    //Funcion que redirige de forma programada
    const redireccionarEdicion = producto => {
        dispatch( obtenerProductoEditar(producto) );
        history.push(`/productos/editar/${id}`);
    };

    return (
        <tr>
            <td>{ nombre }</td>
            <td><span className="font-weight-bold">${ precio }</span></td>
            <td className="acciones">
                <button
                    type="button"
                    onClick={ () => redireccionarEdicion(producto) }
                    className="btn btn-primary mr-2"
                >
                    Editar
                </button>
                <button
                    tyoe="button"
                    className="btn btn-danger"
                    onClick={ () => alertEliminar(id) }
                >
                    Eliminar
                </button>
            </td>
        </tr>
    );

};

export default Producto;