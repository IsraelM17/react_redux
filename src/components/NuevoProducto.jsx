import React, { useState } from 'react';
import {useDispatch, useSelector} from 'react-redux';
import PropTypes from 'prop-types';
//Actions Redux
import { crearNuevoProducto } from "../actions/productoActions";

const NuevoProducto = ({ history }) =>  {

    //state del componente
    const [nombre, guardarNombre] = useState('');
    const [precio, guardarPrecio] = useState(0);

    const dispatch = useDispatch();

    //Acceder al state del store con el hook useSelector
    const cargando  = useSelector( state => state.productos.loading);
    const error     = useSelector(state => state.productos.error);

    //agregar producto
    const agregarProducto = ( producto ) => dispatch( crearNuevoProducto(producto) );

    //Cuando el usuario haga submit
    const submitNuevoProducto = e => {
        e.preventDefault();

        //validar formulario
        if(nombre.trim() === '' || precio <= 0 ){}

        //validar si no hay errores

        //crear nuevo producto
        agregarProducto({ nombre, precio });

        //rediceccionamiento mediante history
        history.push('/');
    };

    console.error(history);

    return (
        <div className='row justify-content-center'>
            <div className="col-md-8">
                <div className="card" >
                    <div className="card-body">
                        <h2 className="text-center mb-4 font-weight-bold">
                            Agregar Nuevo Producto
                        </h2>
                        <form
                            onSubmit={ submitNuevoProducto }
                        >
                            <div className="form-group">
                                <label>Nombre Producto</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre Producto"
                                    name="producto"
                                    value = {nombre}
                                    onChange={ e => guardarNombre(e.target.value)}
                                />
                            </div>
                            <div className="form-group">
                                <label>Precio Producto</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    placeholder="Precio Producto"
                                    name="precio"
                                    value = {precio}
                                    onChange={ e => guardarPrecio( Number(e.target.value)) }
                                />
                                <button
                                    type = "submit"
                                    className="btn btn-primary font-weight-bold text-uppercase d-block w-100"
                                > Agregar </button>

                                { cargando ?  <p> Cargando... </p> : null}
                                { error ? <p className="alert alert-danger p2 mt-4 text-center"> Hubo un error </p> : null }
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

NuevoProducto.propTypes = {};

export default NuevoProducto;