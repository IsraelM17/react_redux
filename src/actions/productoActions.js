import {
    AGREGAR_PRODUCTO,
    AGREGAR_PRODUCTO_EXITO,
    AGREGAR_PRODUCTO_ERROR,
    OBTENER_PRODUCTOS_ERROR,
    OBTENER_PRODUCTOS_EXITO,
    OBTENER_PRODUCTOS_PETICION,
    OBTENER_PRODUCTO_ELIMINAR,
    OBTENER_PRODUCTO_ELIMINAR_EXITO,
    OBTENER_PRODUCTO_ELIMINAR_ERROR, OBTENER_PRODUCTO_EDITAR
} from '../types'

import clienteAxios from "../config/axios";
import Swal from 'sweetalert2';

//Crear nuevos productos
export function crearNuevoProducto(producto) {

    return async (dispatch) => {
        dispatch( agregarProducto() );
        try{
            //Insertar en la Api
            await clienteAxios.post('/productos', producto);
            dispatch( agregarProductoExito(producto) );

            Swal.fire(
                'Correcto',
                'El producto se agrego correctamente',
                'success'
            );
        }catch(error){
            dispatch( agregarProductoError(true) );
            Swal.fire(
                'Error',
                'Ups! Hubo un error',
                'error'
            );
        }
    };

}

const agregarProducto = () => ({
    type: AGREGAR_PRODUCTO,
});

//si el producto se guarda en la db
const agregarProductoExito = producto =>({
    type: AGREGAR_PRODUCTO_EXITO,
    payload: producto
});

//si hubo un error
const agregarProductoError = estado =>({
    type: AGREGAR_PRODUCTO_ERROR,
    payload: estado
});

//funcion que descarga los productos de la base de datos
export function obtenerProductiosAction(){

    return async (dispatch) => {
        dispatch(obtenerProductos());

        try {
            const respuesta = await clienteAxios.get('/productos');
            dispatch(obtenerProductosExito(respuesta.data));
        }catch (e) {
            dispatch(obtenerProductosError());
        }
    };

}

const obtenerProductos = () => ({
    type: OBTENER_PRODUCTOS_PETICION,
    payload: true
});

const obtenerProductosExito =  productos  => ({
    type: OBTENER_PRODUCTOS_EXITO,
    payload: productos
});

const obtenerProductosError = () => ({
    type: OBTENER_PRODUCTOS_ERROR,
    payload: true
});

//Seleccion y eliminia el producto
export function borrarProductoAction (id) {
    return async (dispatch) => {
        dispatch(obtenerProductoEliminar(id));

        try{
            await clienteAxios.delete(`/productos/${id}`);
            dispatch( eliminarProductoExito() );
        } catch (error) {

        }

    }
}

export const obtenerProductoEliminar = id => ({
    type    : OBTENER_PRODUCTO_ELIMINAR,
    payload : id
});

const eliminarProductoExito = () => ({
    type : OBTENER_PRODUCTO_ELIMINAR_EXITO
});

const eliminarProductoError = () => ({
    type    : OBTENER_PRODUCTO_ELIMINAR_ERROR,
    payload : true
});

//Colocar producto edicion
export function obtenerProductoEditar(producto){

    return (dispatch) => {
        dispatch( obtenerProductoAction(producto) );
    };

}

const obtenerProductoAction = producto => ({
    type: OBTENER_PRODUCTO_EDITAR,
    payload: producto
});